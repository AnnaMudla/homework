package com.company;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Scanner sc = new Scanner(System.in);
        String s1, s2;
        s1 = sc.nextLine();
        s2 = sc.nextLine();
        System.out.println("Username: " + s1);
        System.out.println("Initsialy: " + s2);
        System.out.println("Real Time is: " + dateFormat.format(date));
*/
        //OR DEFAULT NAME//

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Scanner sc = new Scanner(System.in);

        String Name = "Mudla Anna";
        String Initsialy = "Mudla A.A";

        System.out.println("Username: " + Name);
        System.out.println("Initsialy: " + Initsialy);
        System.out.println("Real Time is: " + dateFormat.format(date));

    }
}
